let key = require('./key');
const prompt = require('prompt-sync')();
const encryptor = require('simple-encryptor')({
    key: key,
    hmac: false,
    debug: false
});

while (true){
    console.log('\n=======================================================================\n');
    console.log('Hi, Welcome to Etloob URLs Encipher, Select an option to start: \n //1// Encrypt URL. \n //2// Decrypt URL.\n //3// Stop the program');
    let operation = prompt('Option ?');
    if (isNaN(operation)){
        console.log('Enter A Valid Option -___-');
        continue;
    }
    if (parseInt(operation) === 1){
        let url = prompt('Enter URL ?');
        console.log('Encryption Result : \n ' + encryptor.encrypt(url).replace(/\//g,"_") + '\n');
    }
    if (parseInt(operation) === 2){
        let encryptedURL = prompt('Encrypted URL ?');
        console.log('Decrypted URL Result : \n ' + encryptor.decrypt(encryptedURL.replace(/_/g,"/")) + '\n');
    }
    if (parseInt(operation) === 3) {
        break;
    }
}

